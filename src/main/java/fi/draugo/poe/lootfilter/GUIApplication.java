package fi.draugo.poe.lootfilter;

import fi.draugo.poe.lootfilter.gui.events.StageReadyEvent;
import javafx.application.Platform;
import javafx.stage.Stage;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

public class GUIApplication extends javafx.application.Application {

    private ConfigurableApplicationContext applicationContext;

    @Override
    public void start(Stage stage) throws Exception {
        applicationContext.publishEvent(new StageReadyEvent(stage));
    }

    @Override
    public void stop() throws Exception {
        applicationContext.close();
        Platform.exit();
    }

    @Override
    public void init() throws Exception {
        applicationContext = new SpringApplicationBuilder(MainApplication.class).run();
    }

}
