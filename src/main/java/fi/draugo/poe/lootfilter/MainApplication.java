package fi.draugo.poe.lootfilter;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import fi.draugo.poe.lootfilter.enums.Strictness;
import fi.draugo.poe.lootfilter.filter.Filter;
import fi.draugo.poe.lootfilter.output.FilterWriter;
import fi.draugo.poe.lootfilter.output.OutputPacket;
import fi.draugo.poe.lootfilter.util.Constants;
import fi.draugo.poe.lootfilter.util.FilterInfo;
import javafx.application.Application;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.util.StringUtils;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.TRUNCATE_EXISTING;

@SpringBootApplication
public class MainApplication extends ObjectMapper {

    public static ObjectMapper mapper;
    
    private static boolean firstPrint = true;
    
    public static void main(String[] args) {
        //SpringApplication.run(Application.class, args);
        //Application.launch(GUIApplication.class, args);

        /*FilterInfo toggles = readFilterInfo(source_toggles);
        FilterInfo colours = readFilterInfo(source_colours);*/
        List<FilterInfo> filters = new ArrayList<>();
        readFilterInfo(Constants.getDefaultSource(), filters);
        filters.forEach(v -> {
            if(!v.isNoPrint()) {
                v.getStrictness().
                    ifPresentOrElse(s -> printFilter(v, s),
                    () -> Arrays.stream(Strictness.values()).filter(s -> s.getLevel() >= 0).forEach(s -> printFilter(v, s)));
            }
        });

        /*if(filters != null && filters.size() > 0) {
            printFilter(filters, target_starter, Strictness.STARTER);
        }*/
        /*if(toggles != null) {
            printFilter(toggles, target_toggles, Strictness.INFO);
        }
        if(colours != null) {
            printFilter(colours, target_colours, Strictness.INFO);
        }*/
    }

    private static void readFilterInfo(Path path, List<FilterInfo> filters) {
        mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        FilterInfo info = null;
        try {
            String in = Files.readString(path, Constants.charset());
            info = mapper.readValue(in, new TypeReference<FilterInfo>() {});
        } catch (IOException x) {
            x.printStackTrace();
        }
        if(info != null) {
            filters.add(info);
            info.getInitFiles().forEach(v -> readFilterInfo(Paths.get("./"+v+".json"), filters));
        }
    }

    private static void printFilter(FilterInfo info, Strictness strictness) {
        // If more than one info is printed with default filename they will overwrite, not append
        // Get print data
        OutputPacket packet = FilterWriter.print(info, strictness, !firstPrint && !info.isNoPrint());

        // Prepare printing
        String filename = "";
        if(strictness.getLevel() >= 0) {
            filename += strictness.getLevel()+"_";
        }
        filename += strictness.toString()+"-";
        filename += info.getTargetFilename().orElseGet(Constants::defaultFileName);
        Path target = Paths.get("./"+filename+".filter");
        Path changes = Paths.get("./changes_"+filename+".filter");
        // Print Filter
        try (OutputStream out = new BufferedOutputStream(Files.newOutputStream(target, CREATE, TRUNCATE_EXISTING))) {
            byte[] data = packet.getFilter().getBytes();
            out.write(data, 0, data.length);
        } catch (IOException x) {
            x.printStackTrace();
        }

        // Print changes
        if(!firstPrint && StringUtils.hasText(packet.getChanges())) {
            try (OutputStream out = new BufferedOutputStream(Files.newOutputStream(changes, CREATE, TRUNCATE_EXISTING))) {
                byte[] data = packet.getChanges().getBytes();
                out.write(data, 0, data.length);
            } catch (IOException x) {
                x.printStackTrace();
            }
        }

        if(firstPrint) firstPrint=false;
    }
}
