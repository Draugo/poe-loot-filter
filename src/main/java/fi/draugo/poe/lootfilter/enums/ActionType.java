package fi.draugo.poe.lootfilter.enums;

import fi.draugo.poe.lootfilter.interfaces.Command;

public enum ActionType implements Command {
    MINIMAP_ICON("minimapIcon", "MinimapIcon"),
    PLAY_EFFECT("playEffect", "PlayEffect"),
    FONT_SIZE("fontSize", "SetFontSize");

    private final String tag;
    private final String command;

    ActionType(String tag, String command) {
        this.tag = tag;
        this.command = command;
    }

    public String getTag() {
        return tag;
    }

    public String getCommand() {
        return command;
    }

    @Override
    public String toString() {
        return tag;
    }
}
