package fi.draugo.poe.lootfilter.enums;

import com.fasterxml.jackson.annotation.JsonProperty;
import fi.draugo.poe.lootfilter.interfaces.Command;

public enum BlockType implements Command {
    @JsonProperty("show") SHOW("show", "Show"),
    @JsonProperty("hide") HIDE("hide", "Hide"),
    @JsonProperty("continue") CONTINUE("continue", "Continue");

    private final String tag;
    private final String command;

    BlockType(String tag, String command) {
        this.tag = tag;
        this.command = command;
    }

    public String getTag() {
        return tag;
    }

    public String getCommand() {
        return command;
    }

    @Override
    public String toString() {
        return tag;
    }
}
