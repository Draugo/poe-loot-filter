package fi.draugo.poe.lootfilter.enums;

import com.fasterxml.jackson.annotation.JsonProperty;
import fi.draugo.poe.lootfilter.interfaces.Command;

import java.util.function.Predicate;

public enum ClassType implements Command {
    @JsonProperty("quote") QUOTE("quote", "\""), // Used to format multiples together
    @JsonProperty("clear") CLEAR("clear", "Clear"),
    @JsonProperty("gem") GEM("gem", "Gem"),
    @JsonProperty("card") CARD("card", "Card"),
    @JsonProperty("currency") CURRENCY("currency", "Currency"),
    @JsonProperty("flask") FLASK("flask", "Flask"),
    @JsonProperty("jewel") JEWEL("jewel", "Jewel"),
    @JsonProperty("incursion") INCURSION("incursion", "\"Incursion Item\""),
    @JsonProperty("incubator") INCUBATOR("incubator", "Incubator"),
    @JsonProperty("fragment") FRAGMENT("fragment", "Fragment"),
    @JsonProperty("map") MAP("map", "Map"),
    @JsonProperty("sword") SWORD("sword", "Sword"),
    @JsonProperty("dagger") DAGGER("dagger", "Dagger"),
    @JsonProperty("bow") BOW("bow", "Bow"),
    @JsonProperty("axe") AXE("axe", "Axe"),
    @JsonProperty("claw") CLAW("claw", "Claw"),
    @JsonProperty("wand") WAND("wand", "Wand"),
    @JsonProperty("mace") MACE("mace", "Mace"),
    @JsonProperty("sceptre") SCEPTER("sceptre", "Sceptre"),
    @JsonProperty("stave") STAVE("stave", "Stave"),
    @JsonProperty("armour") ARMOUR("armour", "Armour"),
    @JsonProperty("boots") BOOTS("boots", "Boots"),
    @JsonProperty("gloves") GLOVES("gloves", "Gloves"),
    @JsonProperty("helmet") HELMET("helmet", "Helmet"),
    @JsonProperty("shield") SHIELD("shield", "Shield"),
    @JsonProperty("quiver") QUIVER("quiver", "Quiver"),
    @JsonProperty("belt") BELT("belt", "Belt"),
    @JsonProperty("ring") RING("ring", "Ring"),
    @JsonProperty("amulet") AMULET("amulet", "Amulet"),
    @JsonProperty("warstave") WARSTAVE("warstave", "Warstave"),
    @JsonProperty("seed") HARVEST_SEED("seed", "Seed"),
    @JsonProperty("harvest") HARVEST_INFRASTRUCTURE("harvest", "Harvest"),
    @JsonProperty("trinket") TRINKET("trinket", "Trinket"),
    @JsonProperty("contract") CONTRACT("contract", "Contract"),
    @JsonProperty("blueprint") BLUEPRINT("blueprint", "Blueprint"),
    @JsonProperty("quest") QUEST("quest", "Quest"),
    @JsonProperty("heist") HEIST("heist", "Heist"),
    @JsonProperty("target") TARGET("target", "Target"),
    @JsonProperty("cloak") CLOAK("cloak", "Cloak"),
    @JsonProperty("brooch") BROOCH("brooch", "Brooch"),
    @JsonProperty("tool") TOOL("tool", "Tool"),
    @JsonProperty("gear") GEAR("gear", "Gear"),
    @JsonProperty("1hsword") ONE_HAND_SWORD("1hsword", "\"One Hand Sword\""),
    @JsonProperty("thrusting1hsword") THURSTING_ONE_HAND_SWORD("thrusting1hsword", "\"Thrusting One Hand Sword\"");

    private final String tag;
    private final String command;

    ClassType(String tag, String command) {
        this.tag = tag;
        this.command = command;
    }

    public String getTag() {
        return tag;
    }

    public String getCommand() {
        return command;
    }

    public static Predicate<ClassType> isClear() {
        return t -> t.equals(ClassType.CLEAR);
    }

    public String toString() {return tag;}
}
