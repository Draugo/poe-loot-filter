package fi.draugo.poe.lootfilter.enums;

import com.fasterxml.jackson.annotation.JsonProperty;
import fi.draugo.poe.lootfilter.interfaces.Command;

import java.util.Arrays;
import java.util.Optional;
import java.util.function.Predicate;

public enum ColourType implements Command {
    @JsonProperty("clear") CLEAR("clear", "Clear"),
    @JsonProperty("primitive") PRIMITIVE("primitive", "Primitive"),
    @JsonProperty("background") BACKGROUND("background", "SetBackgroundColor"),
    @JsonProperty("border") BORDER("border", "SetBorderColor"),
    @JsonProperty("text") TEXT("text", "SetTextColor");

    private final String tag;
    private final String command;

    ColourType(String tag, String command) {
        this.tag = tag;
        this.command = command;
    }

    public static ColourType fromString(String value) {
        return Arrays.stream(values()).filter(v -> v.tag.equals(value) || v.command.equals(value)).findFirst().orElseThrow(IllegalArgumentException::new);
    }

    public String getTag() {
        return tag;
    }

    public String getCommand() {
        return command;
    }

    public String toString() {return tag;}
}
