package fi.draugo.poe.lootfilter.enums;

import com.fasterxml.jackson.annotation.JsonProperty;
import fi.draugo.poe.lootfilter.interfaces.Command;

public enum ConditionType implements Command {
    BASE_TYPE("baseType", "BaseType"),
    RARITY("rarity", "Rarity"),
    GEM_LEVEL("gemLevel", "GemLevel"),
    QUALITY("quality", "Quality"),
    HAS_MOD("hasMod", "HasExplicitMod"),
    AREA_LEVEL("areaLevel", "AreaLevel"),
    MAP_TIER("mapTier", "MapTier"),
    HEIGHT("height", "Height"),
    WIDTH("width", "Width"),
    SOCKET_GROUP("socketGroup", "SocketGroup"),
    SOCKETS("sockets", "Sockets"),
    LINKED_SOCKETS("linkedSockets", "LinkedSockets"),
    HAS_INFLUENCE("hasInfluence", "HasInfluence"),
    CLASS("class", "Class"),
    ITEM_LEVEL("itemLevel", "ItemLevel"),
    ALTERNATE_QUALITY("alternateQuality", "AlternateQuality"),
    REPLICA("replica", "Replica");

    private final String tag;
    private final String command;

    ConditionType(String tag, String command) {
        this.tag = tag;
        this.command = command;
    }

    public String getTag() {
        return tag;
    }

    public String getCommand() {
        return command;
    }

    @Override
    public String toString() {
        return tag;
    }
}