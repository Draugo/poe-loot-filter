package fi.draugo.poe.lootfilter.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum GroupType {
    @JsonProperty("filter") FILTER("filter", ""),
    @JsonProperty("init") INIT("init", ""),
    @JsonProperty("comment") COMMENT("comment", "");

    private final String tag;
    private final String command;

    GroupType(String tag, String command) {
        this.tag = tag;
        this.command = command;
    }

    public String getTag() {
        return tag;
    }

    public String getCommand() {
        return command;
    }

    public String toString() {return tag;}
}
