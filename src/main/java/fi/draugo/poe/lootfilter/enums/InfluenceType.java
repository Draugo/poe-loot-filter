package fi.draugo.poe.lootfilter.enums;

import com.fasterxml.jackson.annotation.JsonProperty;
import fi.draugo.poe.lootfilter.interfaces.Command;

public enum InfluenceType implements Command {
    @JsonProperty("elder") ELDER("elder", "Elder"),
    @JsonProperty("shaper") SHAPER("shaper", "Shaper"),
    @JsonProperty("crusader") CRUSADER("crusader", "Crusader"),
    @JsonProperty("hunter") HUNTER("hunter", "Hunter"),
    @JsonProperty("redeemer") REDEEMER("redeemer", "Redeemer"),
    @JsonProperty("warlord") WARLORD("warlord", "Warlord");

    private final String tag;
    private final String command;

    InfluenceType(String tag, String command) {
        this.tag = tag;
        this.command = command;
    }

    public String getTag() {
        return tag;
    }

    public String getCommand() {
        return command;
    }

    public String toString() {return tag;}
}
