package fi.draugo.poe.lootfilter.enums;

import com.fasterxml.jackson.annotation.JsonProperty;
import fi.draugo.poe.lootfilter.interfaces.Command;

import java.util.Arrays;

public enum MinimapColour implements Command {
    @JsonProperty("disable") DISABLE("disable", "Disable"),
    @JsonProperty("red") RED("red", "Red"),
    @JsonProperty("green") GREEN("green", "Green"),
    @JsonProperty("blue") BLUE("blue", "Blue"),
    @JsonProperty("brown") BROWN("brown", "Brown"),
    @JsonProperty("white") WHITE("white", "White"),
    @JsonProperty("yellow") YELLOW("yellow", "Yellow"),
    @JsonProperty("cyan") CYAN("cyan", "Cyan"),
    @JsonProperty("gray") GRAY("gray", "Grey"),
    @JsonProperty("orange") ORANGE("orange", "Orange"),
    @JsonProperty("pink") PINK("pink", "Pink"),
    @JsonProperty("purple") PURPLE("purple", "Purple");

    private final String tag;
    private final String command;

    MinimapColour(String tag, String command) {
        this.tag = tag;
        this.command = command;
    }

    public static MinimapColour fromString(String value) {
        return Arrays.stream(values()).filter(v -> v.tag.equals(value) || v.command.equals(value)).findFirst().orElseThrow(IllegalArgumentException::new);
    }

    public String getTag() {
        return tag;
    }

    public String getCommand() {
        return command;
    }

    public String toString() {return tag;}
}
