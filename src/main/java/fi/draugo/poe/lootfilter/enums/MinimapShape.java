package fi.draugo.poe.lootfilter.enums;

import com.fasterxml.jackson.annotation.JsonProperty;
import fi.draugo.poe.lootfilter.interfaces.Command;

import java.util.Arrays;

public enum MinimapShape implements Command {
    @JsonProperty("circle") CIRCLE("circle", "Circle"),
    @JsonProperty("diamond") DIAMOND("diamond", "Diamond"),
    @JsonProperty("hexagon") HEXAGON("hexagon", "Hexagon"),
    @JsonProperty("square") SQUARE("square", "Square"),
    @JsonProperty("star") STAR("star", "Star"),
    @JsonProperty("triangle") TRIANGLE("triangle", "Triangle"),
    @JsonProperty("cross") CROSS("cross", "Cross"),
    @JsonProperty("moon") MOON("moon", "Moon"),
    @JsonProperty("raindrop") RAINDROP("raindrop", "Raindrop"),
    @JsonProperty("kite") KITE("kite", "Kite"),
    @JsonProperty("pentagon") PENTAGON("pentagon", "Pentagon"),
    @JsonProperty("house") HOUSE("house", "UpsideDownHouse");

    private final String tag;
    private final String command;

    MinimapShape(String tag, String command) {
        this.tag = tag;
        this.command = command;
    }

    public static MinimapShape fromString(String value) {
        return Arrays.stream(values()).filter(v -> v.tag.equals(value) || v.command.equals(value)).findFirst().orElseThrow(IllegalArgumentException::new);
    }

    public String getTag() {
        return tag;
    }

    public String getCommand() {
        return command;
    }

    public String toString() {return tag;}
}
