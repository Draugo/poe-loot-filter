package fi.draugo.poe.lootfilter.enums;

import com.fasterxml.jackson.annotation.JsonProperty;
import fi.draugo.poe.lootfilter.interfaces.Command;

import java.util.Arrays;

public enum MinimapSize implements Command {
    @JsonProperty("large") LARGE("large", "0"),
    @JsonProperty("medium") MEDIUM("medium", "1"),
    @JsonProperty("small") SMALL("small", "2"),
    @JsonProperty("disable") DISABLE("disable", "-1");

    private final String tag;
    private final String command;

    MinimapSize(String tag, String command) {
        this.tag = tag;
        this.command = command;
    }

    public static MinimapSize fromString(String value) {
        return Arrays.stream(values()).filter(v -> v.tag.equals(value) || v.command.equals(value)).findFirst().orElseThrow(IllegalArgumentException::new);
    }

    public String getTag() {
        return tag;
    }

    public String getCommand() {
        return command;
    }

    public String toString() {return tag;}
}
