package fi.draugo.poe.lootfilter.enums;

import com.fasterxml.jackson.annotation.JsonProperty;
import fi.draugo.poe.lootfilter.interfaces.Command;

import java.util.Arrays;

public enum OperandType implements Command {
    @JsonProperty("lt") LESS_THAN("lt", "<"),
    @JsonProperty("lteq") LESS_THAN_EQUAL("lteq", "<="),
    @JsonProperty("gt") GREATER_THAN("gt", ">"),
    @JsonProperty("gteq") GREATER_THAN_EQUAL("gteq", ">="),
    @JsonProperty("eq") EQUAL("eq", "=");

    private final String tag;
    private final String command;

    OperandType(String tag, String command) {
        this.tag = tag;
        this.command = command;
    }

    public String getTag() {
        return tag;
    }

    public String getCommand() {
        return command;
    }

    public String toString() {return tag;}

    public static OperandType fromString(String value) {
        return Arrays.stream(values()).filter(v -> v.tag.equals(value) || v.command.equals(value)).findFirst().orElseThrow(IllegalArgumentException::new);
    }
}
