package fi.draugo.poe.lootfilter.enums;

import com.fasterxml.jackson.annotation.JsonProperty;
import fi.draugo.poe.lootfilter.interfaces.Command;

import java.util.Arrays;

public enum RarityType implements Command {
    @JsonProperty("normal") NORMAL("normal", "Normal"),
    @JsonProperty("magic") MAGIC("magic", "Magic"),
    @JsonProperty("rare") RARE("rare", "Rare"),
    @JsonProperty("unique") UNIQUE("unique", "Unique");

    private final String tag;
    private final String command;

    RarityType(String tag, String command) {
        this.tag = tag;
        this.command = command;
    }

    public String getTag() {
        return tag;
    }

    public String getCommand() {
        return command;
    }

    public String toString() {return tag;}

    public static RarityType fromString(String rarity) {
        return Arrays.stream(RarityType.values()).filter(t -> t.tag.equals(rarity) || t.command.equals(rarity)).findFirst().orElseThrow(IllegalArgumentException::new);
    }
}
