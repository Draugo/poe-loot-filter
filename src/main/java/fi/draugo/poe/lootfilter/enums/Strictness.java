package fi.draugo.poe.lootfilter.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public enum Strictness {

    @JsonProperty("info") INFO(-1, "info"),
    @JsonProperty("starter") STARTER(0, "starter"),
    @JsonProperty("early") EARLY(1, "early"),
    @JsonProperty("mid") MID(2, "mid"),
    @JsonProperty("late") LATE(3, "late"),
    @JsonProperty("end") END(4, "end"),
    @JsonProperty("end-strict") END_strict(5, "end-strict");

    private final int level;
    private final String tag;

    Strictness(int level, String tag) {
        this.level = level;
        this.tag = tag;
    }

    public int getLevel() {
        return level;
    }

    public boolean isIncluded (Strictness strictness) {
        return isIncluded(strictness.getLevel());
    }

    public boolean isIncluded(int strictness) {
        return this.level >= strictness;
    }

    public String toString() {
        return tag;
    }
}
