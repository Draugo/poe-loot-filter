package fi.draugo.poe.lootfilter.filter;

import com.fasterxml.jackson.annotation.JsonProperty;
import fi.draugo.poe.lootfilter.MainApplication;
import fi.draugo.poe.lootfilter.filter.actions.FontSize;
import fi.draugo.poe.lootfilter.filter.actions.MinimapIcon;
import fi.draugo.poe.lootfilter.filter.actions.PlayEffect;
import fi.draugo.poe.lootfilter.filter.blocks.Block;
import fi.draugo.poe.lootfilter.group.Group;
import fi.draugo.poe.lootfilter.managers.ColourManager;
import fi.draugo.poe.lootfilter.managers.ToggleManager;
import fi.draugo.poe.lootfilter.enums.*;
import fi.draugo.poe.lootfilter.interfaces.Command;
import fi.draugo.poe.lootfilter.interfaces.Commentable;
import fi.draugo.poe.lootfilter.interfaces.Toggleable;
import fi.draugo.poe.lootfilter.filter.conditions.*;
import fi.draugo.poe.lootfilter.valuetypes.Colour;
import fi.draugo.poe.lootfilter.util.Comment;
import fi.draugo.poe.lootfilter.valuetypes.Value;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Filter implements Commentable, Toggleable {
    private List<Strictness> strictness = new ArrayList<>();
    private Boolean addExtraLine = null;
    private List<Filter> filters = new ArrayList<>();
    // Toggling an entire filter toggles everything within that filter, including other filters
    private boolean enabled = true;

    // Block
    private Block block = null;
    private Comment comment = null;

    // Conditions
    private AreaLevel areaLevel = null;
    private BaseType baseType = null;
    private Rarity rarity = null;
    private GemLevel gemLevel = null;
    private Quality quality = null;
    @JsonProperty("class")
    private ClassCondition classCondition = null;
    private FontSize fontSize = null;
    private HasMod hasMod = null;
    private HasInfluence hasInfluence = null;
    private AlternateQuality alternateQuality = null;
    private Replica replica;
    private MapTier mapTier = null;
    private SocketGroup socketGroup = null;
    private Height height = null;
    private Width width = null;
    private Sockets sockets = null;
    private LinkedSockets linkedSockets = null;
    private ItemLevel itemLevel = null;

    // Actions
    private Colour background = null;
    private Colour border = null;
    private Colour text = null;
    private MinimapIcon minimapIcon = null;
    private PlayEffect playEffect = null;
    @JsonProperty("continue")
    private boolean cont = false;


    public Filter() {}

    @JsonProperty("enabled")
    public void setEnabled(Object enabled) {
        if(enabled instanceof Boolean) {
            this.enabled = (Boolean) enabled;
        } else if(enabled instanceof String) {
            if(Boolean.parseBoolean((String) enabled) || ((String) enabled).equalsIgnoreCase("false")) {
                this.enabled = Boolean.parseBoolean((String) enabled);
            } else {
                ToggleManager.getInstance().load((String) enabled).ifPresent(value -> this.enabled = value.isEnabled());
            }
        }
    }

    public void setAreaLevel(AreaLevel areaLevel) {
        this.areaLevel = areaLevel;
    }

    public void setItemLevel(ItemLevel itemLevel) {
        this.itemLevel = itemLevel;
    }

    @JsonProperty("playEffect")
    public void setPlayEffect(MinimapColour colour) {
        playEffect = new PlayEffect(colour);
    }

    public void setAddExtraLine(Boolean addExtraLine) {
        this.addExtraLine = addExtraLine;
    }

    public void setCont(boolean cont) {
        this.cont = cont;
    }

    public void setComment(Comment comment) {
        this.comment = comment;
    }

    @JsonProperty("block")
    public void setBlock(BlockType block) {
        this.block = new Block(block);
    }

    public void setBlock(Block block) {
        this.block = block;
    }
    public void setRarity(Rarity rarity) {
        this.rarity = rarity;
    }

    public void setFontSize(FontSize fontSize) {
        this.fontSize = fontSize;
    }

    public void setGemLevel(GemLevel gemLevel) {
        this.gemLevel = gemLevel;
    }

    public void setQuality(Quality quality) {
        this.quality = quality;
    }

    public void setMapTier(MapTier mapTier) {
        this.mapTier = mapTier;
    }

    public void setSocketGroup(SocketGroup socketGroup) {
        this.socketGroup = socketGroup;
    }

    public void setHeight(Height height) {
        this.height = height;
    }

    public void setWidth(Width width) {
        this.width = width;
    }

    public void setSockets(Sockets sockets) {
        this.sockets = sockets;
    }

    public void setLinkedSockets(LinkedSockets linkedSockets) {
        this.linkedSockets = linkedSockets;
    }

    @JsonProperty("background")
    public void setBackground(Object background) {
        this.background = deserializeColour(background);
    }

    @JsonProperty("border")
    public void setBorder(Object border) {
        this.border = deserializeColour(border);
    }

    @JsonProperty("text")
    public void setText(Object text) {
        this.text = deserializeColour(text);
    }

    private Colour deserializeColour(Object colour) {
        Colour result = null;
        if(colour instanceof String) {
            result = new Colour();
            if(((String) colour).startsWith("#")) {
                String[] value = ((String) colour).split("#");

                Colour finalResult = result;
                Arrays.stream(value).forEach(c -> {
                    if(c.startsWith("REF")) {
                        finalResult.setRef(c.substring(3));
                    } else if(c.startsWith("R")) {
                        finalResult.setRed(Integer.parseInt(c.substring(1)));
                    } else if(c.startsWith("G")) {
                        finalResult.setGreen(Integer.parseInt(c.substring(1)));
                    } else if(c.startsWith("B")) {
                        finalResult.setBlue(Integer.parseInt(c.substring(1)));
                    } else if(c.startsWith("A")) {
                        finalResult.setAlpha(Integer.parseInt(c.substring(1)));
                    } else if(c.startsWith("C")) {
                        finalResult.setColour(c.substring(1));
                    } else if(c.startsWith("COM")) {
                        finalResult.setComment(new Comment(c.substring(3)));
                    } else if(c.startsWith("T")) {
                        finalResult.setType(ColourType.fromString(c.substring(1)));
                    }
                });
            } else {
                result.setRef(((String) colour));
            }
        } else if (colour instanceof Colour) {
            result = (Colour) colour;
        } else if (colour instanceof LinkedHashMap) {
            result = MainApplication.mapper.convertValue(colour, Colour.class);
        }
        return result;
    }

    @JsonProperty("colourRef")
    public void setColourRef(String value) {
        String[] refs = value.split(" ");
        Arrays.stream(refs).map(ColourManager.getInstance()::load).forEach(oc -> oc.ifPresent(c -> {
            switch(c.getType()) {
                case TEXT:
                    this.setText(c);
                    break;
                case BORDER:
                    this.setBorder(c);
                    break;
                case BACKGROUND:
                    this.setBackground(c);
                    break;
            }
        }));
    }

    @JsonProperty("minimapIcon")
    public void setMinimapIcon(Object minimapIcon) {
        this.minimapIcon = deserializeMinimapIcon(minimapIcon);
    }

    private MinimapIcon deserializeMinimapIcon(Object value) {
        MinimapIcon result = null;
        if(value instanceof String) {
            result = new MinimapIcon();
            if(((String)value).startsWith("#")) {
                String[] icon = ((String) value).split("#");
                MinimapIcon finalResult = result;
                Arrays.stream(icon).forEach(s -> {
                    if(s.startsWith("REF")) {
                        finalResult.setRef(s.substring(3));
                    } else if(s.startsWith("I")) {
                        finalResult.setShape(MinimapShape.fromString(s.substring(1)));
                    } else if(s.startsWith("C")) {
                        finalResult.setColour(MinimapColour.fromString(s.substring(1)));
                    } else if(s.startsWith("S")) {
                        finalResult.setSize(MinimapSize.fromString(s.substring(1)));
                    }
                });
            } else {
                result.setRef((String)value);
            }
        } else if (value instanceof MinimapIcon) {
            result = (MinimapIcon) value;
        } else if (value instanceof LinkedHashMap) {
            result = MainApplication.mapper.convertValue(value, MinimapIcon.class);
        }
        return result;
    }

    public void setPlayEffect(PlayEffect playEffect) {
        this.playEffect = playEffect;
    }

    @JsonProperty("hasMod")
    public void setHasMod(Value value) {
        hasMod = new HasMod();
        hasMod.setValue(value);
    }

    @JsonProperty("hasInfluence")
    public void setHasInfluence(List<InfluenceType> values) {
        this.hasInfluence = new HasInfluence();
        this.hasInfluence.getValues().addAll(values);
    }

    public void setHasInfluence(HasInfluence hasInfluence) {
        this.hasInfluence = hasInfluence;
    }

    @JsonProperty("alternateQuality")
    public void setAlternateQuality(Boolean value) {
        this.alternateQuality = new AlternateQuality(value);
    }
    public void setAlternateQuality(AlternateQuality alternateQuality) {
        this.alternateQuality = alternateQuality;
    }

    @JsonProperty("replica")
    public void setReplica(Boolean value) {
        this.replica = new Replica(value);
    }
    public void setReplica(Replica replica) {
        this.replica = replica;
    }

    public void setHasMod(HasMod hasMod) {
        this.hasMod = hasMod;
    }

    @JsonProperty("fontSize")
    public void setFontSize(Integer fontSize) {
        if(fontSize != null) {
            this.fontSize = new FontSize(fontSize);
        }
    }

    @JsonProperty("class")
    public void setClassCondition(List<ClassType> values) {
        this.classCondition = new ClassCondition();
        this.classCondition.getValues().addAll(values);
    }

    public void setClassCondition(ClassCondition classCondition) {
        this.classCondition = classCondition;
    }

    @JsonProperty("baseType")
    public void setBaseType(List<Value> values) {
        this.baseType = new BaseType();
        this.baseType.getValues().addAll(values);
    }

    public void setBaseType(BaseType baseType) {
        this.baseType = baseType;
    }

    @JsonProperty("filterAction")
    public void setFilterAction(BlockType type) {
        this.block = new Block(type);
    }

    public List<Strictness> getStrictness() {
        return strictness;
    }

    public Optional<Block> getBlock() {
        return Optional.ofNullable(block);
    }

    @Override
    public Optional<Comment> getComment() {
        return Optional.ofNullable(comment);
    }

    public Optional<Boolean> getAddExtraLine() {
        return Optional.ofNullable(addExtraLine);
    }

    public List<Filter> getFilters() {
        return filters;
    }

    public Optional<HasInfluence> getHasInfluence() {
        return Optional.ofNullable(hasInfluence);
    }

    public Optional<AlternateQuality> getAlternateQuality() {
        return Optional.ofNullable(alternateQuality);
    }

    public Optional<Replica> getReplica() {
        return Optional.ofNullable(replica);
    }

    public Optional<AreaLevel> getAreaLevel() {
        return Optional.ofNullable(areaLevel);
    }

    public Optional<ItemLevel> getItemLevel() {
        return Optional.ofNullable(itemLevel);
    }

    public Optional<BaseType> getBaseType() {
        return Optional.ofNullable(baseType);
    }

    public Optional<Rarity> getRarity() {
        return Optional.ofNullable(rarity);
    }

    public Optional<ClassCondition> getClassCondition() {
        return Optional.ofNullable(classCondition);
    }

    public Optional<MapTier> getMapTier() {
        return Optional.ofNullable(mapTier);
    }

    public Optional<GemLevel> getGemLevel() {
        return Optional.ofNullable(gemLevel);
    }

    public Optional<SocketGroup> getSocketGroup() {
        return Optional.ofNullable(socketGroup);
    }

    public Optional<Height> getHeight() {
        return Optional.ofNullable(height);
    }

    public Optional<Width> getWidth() {
        return Optional.ofNullable(width);
    }

    public Optional<Sockets> getSockets() {
        return Optional.ofNullable(sockets);
    }

    public Optional<LinkedSockets> getLinkedSockets() {
        return Optional.ofNullable(linkedSockets);
    }

    public Optional<Quality> getQuality() {
        return Optional.ofNullable(quality);
    }

    public Optional<FontSize> getFontSize() {
        return Optional.ofNullable(fontSize);
    }

    public Optional<Colour> getBackground() {
        return Optional.ofNullable(background);
    }

    public Optional<Colour> getBorder() {
        return Optional.ofNullable(border);
    }

    public Optional<Colour> getText() {
        return Optional.ofNullable(text);
    }

    public Optional<MinimapIcon> getMinimapIcon() {
        return Optional.ofNullable(minimapIcon);
    }

    public Optional<PlayEffect> getPlayEffect() {
        return Optional.ofNullable(playEffect);
    }

    public Optional<HasMod> getHasMod() {
        return Optional.ofNullable(hasMod);
    }

    public boolean isCont() {
        return cont;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public String printComment() {
        StringBuilder sb = new StringBuilder();
        getComment().ifPresent(value -> sb.append(value.print()));
        return sb.toString();
    }

    public List<Command> getCommandStream() {
        return Stream.of(getRarity(), getClassCondition(), getBaseType(), getItemLevel(), getAreaLevel(), getSockets(), getLinkedSockets(), getSocketGroup(), getMapTier(), getHasMod(), getHasInfluence(), getAlternateQuality(), getReplica(), getGemLevel(), getQuality(), getWidth(), getHeight(), getFontSize(), getText(), getBorder(), getBackground(), getMinimapIcon(), getPlayEffect()).map(value -> value.orElse(null)).filter(Objects::nonNull).collect(Collectors.toList());
    }

    public Filter getCurrent(Filter parent) {
        Filter current = new Filter();

        getComment().ifPresent(current::setComment);

        // Block
        getBlock().or(parent::getBlock).ifPresent(current::setBlock);

        // Conditions
        getAreaLevel().or(parent::getAreaLevel).ifPresent(current::setAreaLevel);
        getItemLevel().or(parent::getItemLevel).ifPresent(current::setItemLevel);
        getRarity().or(parent::getRarity).ifPresent(current::setRarity);
        getHasMod().or(parent::getHasMod).ifPresent(current::setHasMod);
        getClassCondition().or(parent::getClassCondition).filter(ClassCondition.isClear().negate()).ifPresent(current::setClassCondition);
        getBaseType().or(parent::getBaseType).ifPresent(current::setBaseType);
        getGemLevel().or(parent::getGemLevel).ifPresent(current::setGemLevel);
        getQuality().or(parent::getQuality).ifPresent(current::setQuality);
        getHasInfluence().or(parent::getHasInfluence).ifPresent(current::setHasInfluence);
        getAlternateQuality().or(parent::getAlternateQuality).ifPresent(current::setAlternateQuality);
        getReplica().or(parent::getReplica).ifPresent(current::setReplica);
        getMapTier().or(parent::getMapTier).ifPresent(current::setMapTier);
        getSocketGroup().or(parent::getSocketGroup).ifPresent(current::setSocketGroup);
        getWidth().or(parent::getWidth).ifPresent(current::setWidth);
        getHeight().or(parent::getHeight).ifPresent(current::setHeight);
        getSockets().or(parent::getSockets).ifPresent(current::setSockets);
        getLinkedSockets().or(parent::getLinkedSockets).ifPresent(current::setLinkedSockets);

        // Actions
        getFontSize().or(parent::getFontSize).ifPresent(current::setFontSize);

        // Sets colours unless colour is clear
        getBackground().or(parent::getBackground).filter(Colour.isClear().negate()).ifPresent(current::setBackground);
        getBorder().or(parent::getBorder).filter(Colour.isClear().negate()).ifPresent(current::setBorder);
        getText().or(parent::getText).filter(Colour.isClear().negate()).ifPresent(current::setText);
        getPlayEffect().or(parent::getPlayEffect).ifPresent(current::setPlayEffect);

        // Build and set current minimap icon
        // This differs from the other cases because Minimap icon contains multiple stats that can be overridden
        MinimapIcon currentIcon = new MinimapIcon();
        List.of(parent.getMinimapIcon(), getMinimapIcon()).forEach(v -> v.ifPresent(currentIcon::override));
        getMinimapIcon().or(parent::getMinimapIcon).ifPresent(v -> current.minimapIcon = currentIcon);
        current.getMinimapIcon().filter(MinimapIcon.isDisabled()).ifPresent(v -> current.setMinimapIcon(null));

        // Others
        if(getStrictness().size() == 0) {
            current.getStrictness().addAll(parent.getStrictness());
        } else {
            current.getStrictness().addAll(getStrictness());
        }
        getAddExtraLine().or(parent::getAddExtraLine).ifPresent(current::setAddExtraLine);

        if(isCont()) {
            current.setCont(true);
        }

        return current;
    }

    public static Filter getCurrent(Group parent) {
        Filter current = new Filter();
        current.getStrictness().addAll(parent.getStrictness());

        return current;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getBlock(), getAreaLevel(), getBaseType(), getRarity(), getGemLevel(), getQuality(), getClassCondition(), getFontSize(), getHasMod(), getHasInfluence(), getAlternateQuality(), getReplica(), getMapTier(), getSocketGroup(), getHeight(), getWidth(), getSockets(), getLinkedSockets(), getItemLevel(), getBackground(), getBorder(), getText(), getMinimapIcon(), getPlayEffect(), isCont());
    }
}
