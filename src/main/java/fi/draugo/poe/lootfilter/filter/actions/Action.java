package fi.draugo.poe.lootfilter.filter.actions;

import fi.draugo.poe.lootfilter.enums.ActionType;

import java.util.Objects;

public abstract class Action {
    private final ActionType type;

    public Action(ActionType type) {
        this.type = type;
    }

    public ActionType getType() {
        return type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getType());
    }
}
