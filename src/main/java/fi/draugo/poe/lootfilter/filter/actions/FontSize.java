package fi.draugo.poe.lootfilter.filter.actions;

import fi.draugo.poe.lootfilter.enums.ActionType;
import fi.draugo.poe.lootfilter.interfaces.Command;

import java.util.Objects;

public class FontSize extends Action implements Command {
    private final Integer value;

    public FontSize(Integer value) {
        super(ActionType.FONT_SIZE);
        this.value = Objects.requireNonNull(value);
    }

    @Override
    public String getCommand() {
        StringBuilder sb = new StringBuilder();
        sb.append(getType().getCommand());
        sb.append(" ").append(value);
        return sb.toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), value);
    }
}
