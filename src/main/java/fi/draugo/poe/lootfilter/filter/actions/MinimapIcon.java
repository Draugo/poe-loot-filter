package fi.draugo.poe.lootfilter.filter.actions;

import com.fasterxml.jackson.annotation.JsonProperty;
import fi.draugo.poe.lootfilter.enums.*;
import fi.draugo.poe.lootfilter.filter.conditions.Condition;
import fi.draugo.poe.lootfilter.interfaces.Command;
import fi.draugo.poe.lootfilter.interfaces.Manageable;
import fi.draugo.poe.lootfilter.interfaces.Toggleable;
import fi.draugo.poe.lootfilter.managers.MinimapIconManager;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MinimapIcon extends Action implements Manageable, Command {
    private MinimapSize size = null;
    private MinimapColour colour = null;
    private MinimapShape shape = null;

    public MinimapIcon() {
        super(ActionType.MINIMAP_ICON);
    }

    @JsonProperty("id")
    public void setId(String id) {
        MinimapIconManager.getInstance().register(id, this);
    }

    @JsonProperty("ref")
    public void setRef(String ref) {
        Optional<MinimapIcon> o_icon = MinimapIconManager.getInstance().load(ref);
        if(o_icon.isPresent()) {
            MinimapIcon icon = o_icon.get();
            if(getSize().isEmpty()) size = icon.size;
            if(getColour().isEmpty()) colour = icon.colour;
            if(getShape().isEmpty()) shape = icon.shape;
        }
    }

    public void setSize(MinimapSize size) {
        this.size = size;
    }

    public void setColour(MinimapColour colour) {
        this.colour = colour;
    }

    public void setShape(MinimapShape shape) {
        this.shape = shape;
    }

    public Optional<MinimapSize> getSize() {
        return Optional.ofNullable(size);
    }

    public Optional<MinimapColour> getColour() {
        return Optional.ofNullable(colour);
    }

    public Optional<MinimapShape> getShape() {
        return Optional.ofNullable(shape);
    }

    public List<Command> getAttributes() {
        return Stream.of(getSize(), getColour(), getShape()).map(value -> value.orElse(null)).filter(Objects::nonNull).collect(Collectors.toList());
    }

    @Override
    public String getCommand() {
        return getCommand(false);
    }

    public String getCommand(boolean skipCheck) {
        if(!skipCheck) {
            Assert.isTrue(getSize().isPresent(), "Size is mandatory for MinimapIcon command");
            Assert.isTrue(getColour().isPresent(), "Colour is mandatory for MinimapIcon command");
            Assert.isTrue(getShape().isPresent(), "Shape is mandatory for MinimapIcon command");
        }
        StringBuilder sb = new StringBuilder();
        sb.append(getType().getCommand());
        getAttributes().forEach(value -> sb.append(" ").append(value.getCommand()));
        return sb.toString();
    }

    public void override(MinimapIcon other) {
        other.getShape().ifPresent(this::setShape);
        other.getColour().ifPresent(this::setColour);
        other.getSize().ifPresent(this::setSize);
    }

    public static Predicate<MinimapIcon> isDisabled() {
        return i -> i.getSize().filter(v -> v.equals(MinimapSize.DISABLE)).isPresent();
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getSize(), getColour(), getShape());
    }
}
