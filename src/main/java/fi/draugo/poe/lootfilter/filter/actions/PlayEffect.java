package fi.draugo.poe.lootfilter.filter.actions;

import fi.draugo.poe.lootfilter.enums.ActionType;
import fi.draugo.poe.lootfilter.enums.ConditionType;
import fi.draugo.poe.lootfilter.enums.MinimapColour;
import fi.draugo.poe.lootfilter.interfaces.Command;
import fi.draugo.poe.lootfilter.interfaces.Manageable;
import fi.draugo.poe.lootfilter.managers.PlayEffectManager;
import org.springframework.util.Assert;

import java.util.Objects;
import java.util.Optional;

public class PlayEffect extends Action implements Manageable, Command {
    // TODO: Implement temp visualization
    private MinimapColour colour;


    public PlayEffect(MinimapColour colour) {
        super(ActionType.PLAY_EFFECT);
        this.colour = colour;
    }

    public void setId(String id) {
        PlayEffectManager.getInstance().register(id, this);
    }

    public void setRef(String ref) {
        Optional<PlayEffect> o_playeffect = PlayEffectManager.getInstance().load(ref);
        o_playeffect.ifPresent(p -> {if(getColour().isEmpty()) colour = p.colour;});
    }

    public Optional<MinimapColour> getColour() {
        return Optional.ofNullable(colour);
    }

    @Override
    public String getCommand() {
        return getCommand(false);
    }

    public String getCommand(boolean skipCheck) {
        if(!skipCheck) Assert.isTrue(getColour().isPresent(), "Colour is mandatory for PlayEffect command");
        StringBuilder sb = new StringBuilder();
        sb.append(getType().getCommand()).append(" ").append(getColour().get().getCommand());
        return sb.toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getColour());
    }
}
