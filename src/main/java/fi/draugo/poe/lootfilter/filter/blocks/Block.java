package fi.draugo.poe.lootfilter.filter.blocks;

import com.fasterxml.jackson.annotation.JsonProperty;
import fi.draugo.poe.lootfilter.enums.BlockType;
import fi.draugo.poe.lootfilter.interfaces.Command;

import java.util.Objects;
import java.util.Optional;

public class Block implements Command{
    private BlockType type;

    public Block(BlockType type) {
        this.type = type;
    }

    public Optional<BlockType> getType() {
        return Optional.ofNullable(type);
    }

    @Override
    public String getCommand() {
        Objects.requireNonNull(type, "Type is mandatory for Block command");
        StringBuilder sb = new StringBuilder();
        sb.append(type.getCommand());
        return sb.toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getType());
    }
}
