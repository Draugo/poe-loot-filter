package fi.draugo.poe.lootfilter.filter.conditions;

import fi.draugo.poe.lootfilter.enums.ConditionType;
import fi.draugo.poe.lootfilter.enums.InfluenceType;
import fi.draugo.poe.lootfilter.interfaces.Command;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class AlternateQuality extends Condition implements Command {
    private Boolean value = false;

    public AlternateQuality() {
        super(ConditionType.ALTERNATE_QUALITY);
    }
    public AlternateQuality(Boolean value) {
        super(ConditionType.ALTERNATE_QUALITY);
        this.value = value;
    }

    public Boolean getValue() {
        return value;
    }

    @Override
    public String getCommand() {
        StringBuilder sb = new StringBuilder();
        sb.append(getType().getCommand());
        sb.append(" ").append(getValue());
        return sb.toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getValue());
    }
}
