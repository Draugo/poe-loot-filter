package fi.draugo.poe.lootfilter.filter.conditions;

import fi.draugo.poe.lootfilter.enums.ConditionType;
import fi.draugo.poe.lootfilter.interfaces.Command;

import java.util.Objects;

public class AreaLevel extends ComparableValueCondition implements Command {
    public AreaLevel(Integer value) {
        super(ConditionType.AREA_LEVEL, value);
    }
    public AreaLevel(String value) {
        super(ConditionType.AREA_LEVEL, value);
    }
}
