package fi.draugo.poe.lootfilter.filter.conditions;

import fi.draugo.poe.lootfilter.enums.ConditionType;
import fi.draugo.poe.lootfilter.interfaces.Command;
import fi.draugo.poe.lootfilter.valuetypes.Value;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class BaseType extends Condition implements Command {
    private List<Value> values = new ArrayList<>();

    public BaseType() {
        super(ConditionType.BASE_TYPE);
    }

    public List<Value> getValues() {
        return values;
    }

    @Override
    public String getCommand() {
        StringBuilder sb = new StringBuilder();
        sb.append(getType().getCommand());
        values.stream().filter(Value::isEnabled).forEach(value -> sb.append(" ").append(value.getCommand()));
        return sb.toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getValues());
    }
}
