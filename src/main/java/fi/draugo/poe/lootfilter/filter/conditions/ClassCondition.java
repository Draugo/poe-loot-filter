package fi.draugo.poe.lootfilter.filter.conditions;

import fi.draugo.poe.lootfilter.enums.ClassType;
import fi.draugo.poe.lootfilter.enums.ConditionType;
import fi.draugo.poe.lootfilter.interfaces.Command;
import fi.draugo.poe.lootfilter.valuetypes.Value;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

public class ClassCondition extends Condition implements Command {
    private List<ClassType> values = new ArrayList<>();

    public ClassCondition() {
        super(ConditionType.CLASS);
    }

    public List<ClassType> getValues() {
        return values;
    }

    @Override
    public String getCommand() {
        StringBuilder sb = new StringBuilder();
        sb.append(getType().getCommand());
        values.forEach(value -> sb.append(" ").append(value.getCommand()));
        return sb.toString();
    }

    public static Predicate<ClassCondition> isClear() {
        return t -> t.values.stream().anyMatch(ClassType.isClear());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getValues());
    }
}
