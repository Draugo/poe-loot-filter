package fi.draugo.poe.lootfilter.filter.conditions;

import fi.draugo.poe.lootfilter.enums.ConditionType;
import fi.draugo.poe.lootfilter.enums.OperandType;
import fi.draugo.poe.lootfilter.interfaces.Command;

import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;

public abstract class ComparableValueCondition extends Condition implements Command {
    private Integer value;
    private OperandType operand;

    public ComparableValueCondition(ConditionType type) {
        super(type);
    }
    public ComparableValueCondition(ConditionType type, Integer value) {
        super(type);
        this.value = value;
    }
    public ComparableValueCondition(ConditionType type, String value) {
        super(type);
        String[] split = value.split(" ");
        if(split.length > 2) {
            throw new IllegalArgumentException("ComparableValueCondition requires at most two parameters");
        }
        if(split.length == 1) {
            this.value = Integer.parseInt(split[0]);
        } else {
            this.operand = Arrays.stream(OperandType.values()).filter(t -> t.getTag().equals(split[0]) || t.getCommand().equals(split[0])).findFirst().orElseThrow(IllegalArgumentException::new);
            this.value = Integer.parseInt(split[1]);
        }
    }

    public Integer getValue() {
        return value;
    }

    public Optional<OperandType> getOperand() {
        return Optional.ofNullable(operand);
    }

    @Override
    public String getCommand() {
        Objects.requireNonNull(value, "Value is mandatory for ComparableCondition command");
        StringBuilder sb = new StringBuilder();
        sb.append(getType().getCommand()).append(" ");
        getOperand().ifPresent(o -> sb.append(o.getCommand()).append(" "));
        sb.append(value);
        return sb.toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getValue(), getOperand());
    }
}
