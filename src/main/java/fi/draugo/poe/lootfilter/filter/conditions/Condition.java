package fi.draugo.poe.lootfilter.filter.conditions;

import fi.draugo.poe.lootfilter.enums.ConditionType;

import java.util.Objects;

public abstract class Condition {
    private final ConditionType type;

    public Condition(ConditionType type) {
        this.type = type;
    }

    public ConditionType getType() {
        return type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getType());
    }
}
