package fi.draugo.poe.lootfilter.filter.conditions;

import fi.draugo.poe.lootfilter.enums.ConditionType;
import fi.draugo.poe.lootfilter.interfaces.Command;

public class GemLevel extends ComparableValueCondition implements Command {
    public GemLevel(Integer value) {
        super(ConditionType.GEM_LEVEL, value);
    }
    public GemLevel(String value) {
        super(ConditionType.GEM_LEVEL, value);
    }
}
