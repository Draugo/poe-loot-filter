package fi.draugo.poe.lootfilter.filter.conditions;

import fi.draugo.poe.lootfilter.enums.ConditionType;
import fi.draugo.poe.lootfilter.enums.InfluenceType;
import fi.draugo.poe.lootfilter.interfaces.Command;
import fi.draugo.poe.lootfilter.valuetypes.Value;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class HasInfluence extends Condition implements Command {
    private List<InfluenceType> values = new ArrayList<>();

    public HasInfluence() {
        super(ConditionType.HAS_INFLUENCE);
    }

    public List<InfluenceType> getValues() {
        return values;
    }

    @Override
    public String getCommand() {
        StringBuilder sb = new StringBuilder();
        sb.append(getType().getCommand());
        values.forEach(value -> sb.append(" ").append(value.getCommand()));
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HasInfluence that = (HasInfluence) o;
        return getValues().equals(that.getValues());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getValues());
    }
}
