package fi.draugo.poe.lootfilter.filter.conditions;

import fi.draugo.poe.lootfilter.enums.ConditionType;
import fi.draugo.poe.lootfilter.interfaces.Command;
import fi.draugo.poe.lootfilter.valuetypes.Value;
import org.springframework.util.Assert;

import java.util.Objects;
import java.util.Optional;

public class HasMod extends Condition implements Command {
    private Value value = null;

    public HasMod() {
        super(ConditionType.HAS_MOD);
    }
    public HasMod(String value) {
        super(ConditionType.HAS_MOD);
        this.value = new Value(value);
    }

    public void setValue(Value value) {
        this.value = value;
    }

    public Optional<Value> getValue() {
        return Optional.ofNullable(value);
    }

    @Override
    public String getCommand() {
        Assert.isTrue(getValue().isPresent(), "Value is mandatory for HasMod command");
        StringBuilder sb = new StringBuilder();
        sb.append(getType().getCommand()).append(" ").append(getValue().get().getCommand());
        return sb.toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getValue());
    }
}
