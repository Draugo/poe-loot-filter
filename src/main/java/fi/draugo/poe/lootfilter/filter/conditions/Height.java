package fi.draugo.poe.lootfilter.filter.conditions;

import fi.draugo.poe.lootfilter.enums.ConditionType;
import fi.draugo.poe.lootfilter.interfaces.Command;

public class Height extends ComparableValueCondition implements Command {
    public Height(Integer value) {
        super(ConditionType.HEIGHT, value);
    }
    public Height(String value) {
        super(ConditionType.HEIGHT, value);
    }
}
