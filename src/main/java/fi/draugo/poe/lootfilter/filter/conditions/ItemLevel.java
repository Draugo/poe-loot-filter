package fi.draugo.poe.lootfilter.filter.conditions;

import fi.draugo.poe.lootfilter.enums.ConditionType;
import fi.draugo.poe.lootfilter.interfaces.Command;

public class ItemLevel extends ComparableValueCondition implements Command {
    public ItemLevel(Integer value) {
        super(ConditionType.ITEM_LEVEL, value);
    }
    public ItemLevel(String value) {
        super(ConditionType.ITEM_LEVEL, value);
    }
}
