package fi.draugo.poe.lootfilter.filter.conditions;

import fi.draugo.poe.lootfilter.enums.ConditionType;
import fi.draugo.poe.lootfilter.interfaces.Command;

public class LinkedSockets extends ComparableValueCondition implements Command {
    public LinkedSockets(Integer value) {
        super(ConditionType.LINKED_SOCKETS, value);
    }
    public LinkedSockets(String value) {
        super(ConditionType.LINKED_SOCKETS, value);
    }
}
