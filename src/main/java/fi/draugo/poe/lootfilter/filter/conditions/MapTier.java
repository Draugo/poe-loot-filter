package fi.draugo.poe.lootfilter.filter.conditions;

import fi.draugo.poe.lootfilter.enums.ConditionType;
import fi.draugo.poe.lootfilter.interfaces.Command;

public class MapTier extends ComparableValueCondition implements Command {
    public MapTier(Integer value) {
        super(ConditionType.MAP_TIER, value);
    }
    public MapTier(String value) {
        super(ConditionType.MAP_TIER, value);
    }
}
