package fi.draugo.poe.lootfilter.filter.conditions;

import fi.draugo.poe.lootfilter.enums.ConditionType;
import fi.draugo.poe.lootfilter.interfaces.Command;

public class Quality extends ComparableValueCondition implements Command {

    public Quality(Integer value) {
        super(ConditionType.QUALITY, value);
    }
    public Quality(String value) {
        super(ConditionType.QUALITY, value);
    }
}
