package fi.draugo.poe.lootfilter.filter.conditions;

import fi.draugo.poe.lootfilter.enums.ConditionType;
import fi.draugo.poe.lootfilter.enums.OperandType;
import fi.draugo.poe.lootfilter.enums.RarityType;
import fi.draugo.poe.lootfilter.interfaces.Command;

import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;

public class Rarity extends Condition implements Command {
    private RarityType rarity;
    private OperandType operand;

    public Rarity() {
        super(ConditionType.RARITY);
    }

    public Rarity(String value) {
        super(ConditionType.RARITY);
        String[] split = value.split(" ");
        if(split.length == 1) {
            this.rarity = RarityType.fromString(split[0]);
        } else if(split.length == 2) {
            this.operand = OperandType.fromString(split[0]);
            this.rarity = RarityType.fromString(split[1]);
        } else {
            throw new IllegalArgumentException("Rarity requires one or two parameters");
        }
    }

    public RarityType getRarity() {
        return rarity;
    }

    public Optional<OperandType> getOperand() {
        return Optional.ofNullable(operand);
    }

    @Override
    public String getCommand() {
        Objects.requireNonNull(rarity, "Rarity is mandatory for Rarity command");
        StringBuilder sb = new StringBuilder();
        sb.append(getType().getCommand());
        getOperand().ifPresent(o -> sb.append(" ").append(o.getCommand()));
        sb.append(" ").append(rarity.getCommand());
        return sb.toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getRarity(), getOperand());
    }
}
