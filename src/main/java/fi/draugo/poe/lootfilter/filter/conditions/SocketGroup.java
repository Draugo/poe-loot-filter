package fi.draugo.poe.lootfilter.filter.conditions;

import fi.draugo.poe.lootfilter.enums.ConditionType;
import fi.draugo.poe.lootfilter.interfaces.Command;

import java.util.Objects;

public class SocketGroup extends Condition implements Command {
    private final String group;

    public SocketGroup(String group) {
        super(ConditionType.SOCKET_GROUP);
        this.group = Objects.requireNonNull(group);
    }

    @Override
    public String getCommand() {
        StringBuilder sb = new StringBuilder();
        sb.append(getType().getCommand());
        sb.append(" ").append(group);
        return sb.toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), group);
    }
}
