package fi.draugo.poe.lootfilter.filter.conditions;

import fi.draugo.poe.lootfilter.enums.ConditionType;
import fi.draugo.poe.lootfilter.interfaces.Command;

import java.util.Objects;

public class Sockets extends ComparableValueCondition implements Command {

    public Sockets(Integer value) {
        super(ConditionType.SOCKETS, value);
    }
    public Sockets(String value) {
        super(ConditionType.SOCKETS, value);
    }
}
