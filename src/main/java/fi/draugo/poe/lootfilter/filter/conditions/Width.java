package fi.draugo.poe.lootfilter.filter.conditions;

import fi.draugo.poe.lootfilter.enums.ConditionType;
import fi.draugo.poe.lootfilter.interfaces.Command;

public class Width extends ComparableValueCondition implements Command {
    public Width(Integer value) {
        super(ConditionType.WIDTH, value);
    }
    public Width(String value) {
        super(ConditionType.WIDTH, value);
    }
}
