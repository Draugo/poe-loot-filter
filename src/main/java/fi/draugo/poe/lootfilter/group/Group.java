package fi.draugo.poe.lootfilter.group;

import com.fasterxml.jackson.annotation.JsonProperty;
import fi.draugo.poe.lootfilter.util.Comment;
import fi.draugo.poe.lootfilter.filter.Filter;
import fi.draugo.poe.lootfilter.filter.actions.MinimapIcon;
import fi.draugo.poe.lootfilter.filter.actions.PlayEffect;
import fi.draugo.poe.lootfilter.managers.ToggleManager;
import fi.draugo.poe.lootfilter.enums.GroupType;
import fi.draugo.poe.lootfilter.enums.Strictness;
import fi.draugo.poe.lootfilter.interfaces.Toggleable;
import fi.draugo.poe.lootfilter.util.Toggle;
import fi.draugo.poe.lootfilter.valuetypes.Colour;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Encapsulates a list of related filters and provides a group description.
 * Can contain initialization information such as colours.
 * These can be printed of not depending on the strictness settings.
 *
 * Option for nested Groups is given but groups containing other groups never have their
 * filters printed so keep that in mind.
 */
public class Group implements Toggleable {
    private List<Strictness> strictness = new ArrayList<>();
    private GroupType type;
    private boolean enabled = true;
    private boolean forceMultiline = false;
    private List<Comment> description = new ArrayList<>();
    private List<Colour> colours = new ArrayList<>();
    private List<Toggle> toggles = new ArrayList<>();
    private List<Filter> filters = new ArrayList<>();
    private List<Group> groups = new ArrayList<>();
    private List<MinimapIcon> icons = new ArrayList<>();
    private List<PlayEffect> effects = new ArrayList<>();

    public Group() {}

    @JsonProperty("enabled")
    public void setEnabled(Object enabled) {
        if(enabled instanceof Boolean) {
            this.enabled = (Boolean) enabled;
        } else if(enabled instanceof String) {
            if(Boolean.parseBoolean((String) enabled) || ((String) enabled).equalsIgnoreCase("false")) {
                this.enabled = Boolean.parseBoolean((String) enabled);
            } else {
                ToggleManager.getInstance().load((String) enabled).ifPresent(value -> this.enabled = value.isEnabled());
            }
        }
    }

    public void setForceMultiline(boolean forceMultiline) {
        this.forceMultiline = forceMultiline;
    }

    public void setType(GroupType type) {
        this.type = type;
    }

    public List<Strictness> getStrictness() {
        return strictness;
    }

    public Optional<GroupType> getType() {
        return Optional.ofNullable(type);
    }

    public List<Comment> getDescription() {
        return description;
    }

    public List<Colour> getColours() {
        return colours;
    }

    public List<Toggle> getToggles() {
        return toggles;
    }

    public List<Filter> getFilters() {
        return filters;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public List<MinimapIcon> getIcons() {
        return icons;
    }

    public List<PlayEffect> getEffects() {
        return effects;
    }

    public boolean getForceMultiline() {
        return forceMultiline;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public Group getCurrent(Group parent) {
        Group current = new Group();
        if(getStrictness().size() == 0) {
            current.getStrictness().addAll(parent.getStrictness());
        } else {
            current.getStrictness().addAll(getStrictness());
        }

        getType().or(parent::getType).ifPresentOrElse(current::setType, () -> current.setType(null));

        return current;
    }
}
