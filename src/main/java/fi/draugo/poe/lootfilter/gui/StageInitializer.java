package fi.draugo.poe.lootfilter.gui;

import fi.draugo.poe.lootfilter.gui.events.StageReadyEvent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import javafx.stage.StageStyle;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class StageInitializer implements ApplicationListener<StageReadyEvent> {

    @Override
    public void onApplicationEvent(StageReadyEvent event) {
        Stage stage = event.getStage();

        stage.initStyle(StageStyle.UNIFIED);

        String javaVersion = System.getProperty("java.version");
        String javafxVersion = System.getProperty("javafx.version");
        Label l = new Label("Hello, JavaFX " + javafxVersion + ", running on Java " + javaVersion + ".");
        stage.setTitle("Hello, JavaFX " + javafxVersion + ", running on Java " + javaVersion);
        //Scene scene = new Scene(new StackPane(l), 640, 480);
        //stage.setScene(scene);
        stage.show();

    }
}
