package fi.draugo.poe.lootfilter.interfaces;

@FunctionalInterface
public interface Command {
    String getCommand();
}
