package fi.draugo.poe.lootfilter.interfaces;

import fi.draugo.poe.lootfilter.util.Comment;

import java.util.Optional;

public interface Commentable {
    Optional<Comment> getComment();
    String printComment();
}
