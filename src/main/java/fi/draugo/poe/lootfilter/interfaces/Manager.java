package fi.draugo.poe.lootfilter.interfaces;

import java.util.Optional;

public interface Manager<T extends Manageable> {
    public void register(String id, T item);
    public Optional<T> load(String id);
}
