package fi.draugo.poe.lootfilter.interfaces;

public interface Toggleable {
    boolean isEnabled();
}
