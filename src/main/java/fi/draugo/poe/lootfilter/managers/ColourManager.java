package fi.draugo.poe.lootfilter.managers;

import fi.draugo.poe.lootfilter.interfaces.Manager;
import fi.draugo.poe.lootfilter.valuetypes.Colour;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class ColourManager implements Manager<Colour> {
    private static ColourManager instance = null;

    private ColourManager() {}
    public static ColourManager getInstance() {
        if(instance == null) {
            instance = new ColourManager();
        }
        return instance;
    }

    private Map<String, Colour> content = new HashMap<>();

    public void register(String id, Colour colour) {
        content.put(id, colour);
    }

    public Optional<Colour> load(String id) {
        return Optional.of(content.get(id));
    }
}
