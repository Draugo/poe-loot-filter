package fi.draugo.poe.lootfilter.managers;

import fi.draugo.poe.lootfilter.interfaces.Manager;
import fi.draugo.poe.lootfilter.filter.actions.MinimapIcon;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class MinimapIconManager implements Manager<MinimapIcon> {
    private static MinimapIconManager instance = null;

    private MinimapIconManager() {}
    public static MinimapIconManager getInstance() {
        if(instance == null) {
            instance = new MinimapIconManager();
        }
        return instance;
    }

    private Map<String, MinimapIcon> content = new HashMap<>();

    public void register(String id, MinimapIcon icon) {
        content.put(id, icon);
    }

    public Optional<MinimapIcon> load(String id) {
        return Optional.ofNullable(content.get(id));
    }
}
