package fi.draugo.poe.lootfilter.managers;

import fi.draugo.poe.lootfilter.interfaces.Manager;
import fi.draugo.poe.lootfilter.filter.actions.PlayEffect;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class PlayEffectManager implements Manager<PlayEffect> {
    private static PlayEffectManager instance = null;

    private PlayEffectManager() {}
    public static PlayEffectManager getInstance() {
        if(instance == null) {
            instance = new PlayEffectManager();
        }
        return instance;
    }

    private Map<String, PlayEffect> content = new HashMap<>();

    public void register(String id, PlayEffect effect) {
        content.put(id, effect);
    }

    public Optional<PlayEffect> load(String id) {
        return Optional.of(content.get(id));
    }
}
