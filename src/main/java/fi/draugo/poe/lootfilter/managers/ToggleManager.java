package fi.draugo.poe.lootfilter.managers;

import fi.draugo.poe.lootfilter.interfaces.Manager;
import fi.draugo.poe.lootfilter.util.Toggle;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class ToggleManager implements Manager<Toggle> {
    private static ToggleManager instance = null;

    private ToggleManager() {}
    public static ToggleManager getInstance() {
        if(instance == null) {
            instance = new ToggleManager();
        }
        return instance;
    }

    private Map<String, Toggle> content = new HashMap<>();

    public void register(String id, Toggle toggle) {
        content.put(id, toggle);
    }

    public Optional<Toggle> load(String id) {
        return Optional.of(content.get(id));
    }
}
