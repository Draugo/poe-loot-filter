package fi.draugo.poe.lootfilter.output;

import fi.draugo.poe.lootfilter.enums.Strictness;
import fi.draugo.poe.lootfilter.interfaces.Command;
import fi.draugo.poe.lootfilter.filter.Filter;
import fi.draugo.poe.lootfilter.group.Group;
import fi.draugo.poe.lootfilter.util.FilterInfo;
import org.springframework.util.Assert;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class FilterWriter {

    private static Map<Integer, Filter> previousMap = new HashMap<>();
    private static Map<Integer, Filter> currentMap = new HashMap<>();
    private static Map<Integer, Filter> changesMap = new HashMap<>();

    public static OutputPacket print(FilterInfo info, Strictness printStrictness, boolean printChanges) {
        OutputPacket packet = new OutputPacket();
        Objects.requireNonNull(printStrictness, "Strictness is mandatory for Print");
        StringBuilder sb = new StringBuilder();

        info.getGroups().stream().filter(Group::isEnabled).map(group -> print(group, new Group(), printStrictness)).forEach(sb::append);
        packet.setFilter(sb.toString());

        if(printChanges && !previousMap.isEmpty()) {
            currentMap.values().forEach(f -> {
                if(!previousMap.containsKey(f.hashCode())) {
                    changesMap.put(f.hashCode(), f);
                } else {
                    previousMap.remove(f.hashCode());
                }
            });
        }

        // Has additions and removals
        sb = new StringBuilder();
        sb.append("Strictness: ").append(printStrictness.name()).append(System.lineSeparator());
        sb.append("Removals").append(System.lineSeparator());
        previousMap.values().stream().map(FilterWriter::print).forEach(sb::append);
        sb.append(System.lineSeparator());
        sb.append("Additions").append(System.lineSeparator());
        changesMap.values().stream().map(FilterWriter::print).forEach(sb::append);
        packet.setChanges(sb.toString());

        // Cleanup
        previousMap = currentMap;
        currentMap = new HashMap<>();
        changesMap = new HashMap<>();
        return packet;
    }

    public static String print(List<Group> groups, Group parent, Strictness printStrictness, boolean printChanges) {
        Objects.requireNonNull(printStrictness, "Strictness is mandatory for Print");
        Group current = parent == null ? new Group() : parent;
        StringBuilder sb = new StringBuilder();

        groups.stream().filter(Group::isEnabled).map(group -> print(group, current, printStrictness)).forEach(sb::append);
        return sb.toString();
    }

    private static String print(Group group, Group parent, Strictness printStrictness) {
        Group current = group.getCurrent(parent);
        Assert.isTrue(current.getStrictness().size() > 0, "Strictness is mandatory for Print");
        StringBuffer sb = new StringBuffer();
        if(current.getStrictness().contains(printStrictness)) {
            if (group.getDescription().size() == 1 && !group.getForceMultiline()) {
                // Print single line description
                sb.append(group.getDescription().get(0).print()).append(System.lineSeparator());
            } else if (group.getDescription().size() > 1 || (group.getDescription().size() == 1 && group.getForceMultiline())) {
                // Print multiline description
                sb.append("# ------------------------------------------------------------").
                        append(System.lineSeparator());
                group.getDescription().forEach(c -> sb.append(c.print()).append(System.lineSeparator()));
                sb.append("# ------------------------------------------------------------").
                        append(System.lineSeparator());
            }
            // All group colours
            group.getColours().forEach(value -> sb.append("# ").append(value.getCommand()).append(System.lineSeparator()));
            // All group toggles
            group.getToggles().forEach(value -> sb.append("# ").append(value.getCommand()).append(System.lineSeparator()));
            // All group icons
            group.getIcons().forEach(value -> sb.append("# ").append(value.getCommand(true)).append(System.lineSeparator()));
            // All group effects
            group.getEffects().forEach(value -> sb.append("# ").append(value.getCommand()).append(System.lineSeparator()));
        }
        if(group.getGroups().isEmpty()) {
            group.getFilters().stream().filter(Filter::isEnabled).map(filter -> print(filter, Filter.getCurrent(current), printStrictness)).forEach(sb::append);
        } else {
            group.getGroups().stream().filter(Group::isEnabled).map(g -> print(g, current, printStrictness)).forEach(sb::append);
        }
        if((group.getGroups().isEmpty() && group.getFilters().isEmpty()) || !group.getColours().isEmpty()) {
            sb.append(System.lineSeparator());
        }
        return sb.toString();
    }

    private static String print(Filter filter, Filter parent, Strictness printStrictness) {
        Filter current = filter.getCurrent(parent);
        StringBuilder sb = new StringBuilder();

        if(filter.getFilters().isEmpty()) {
            Assert.isTrue(current.getStrictness().size() > 0, "Strictness is mandatory for printing");
            if(current.getStrictness().contains(printStrictness)) {
                sb.append(print(current));
            }
        } else {
            filter.getFilters().stream().filter(Filter::isEnabled).forEach(f -> sb.append(print(f, current, printStrictness)));
        }
        return sb.toString();
    }

    private static String print(Filter current) {
        StringBuilder sb = new StringBuilder();

        Consumer<Command> append = n -> sb.append("  ").append(n.getCommand()).append(System.lineSeparator());

        // Filter Action [Show|Hide]
        current.getBlock().ifPresent(value -> sb.append(value.getCommand()));

        // Filter Comment
        current.getComment().ifPresent(value -> {
            current.getBlock().ifPresent(fa -> sb.append(" "));
            sb.append(value.print());
        });

        if(Stream.of(current.getBlock(), current.getComment()).anyMatch(Optional::isPresent)) {
            sb.append(System.lineSeparator());
        }

        // Print streamable commands
        current.getCommandStream().forEach(append);

        if(current.isCont()) {
            sb.append("  ").append("Continue").append(System.lineSeparator());
        }

        // Extra line if not specifically suppressed
        current.getAddExtraLine().ifPresentOrElse(
                value -> {if(value) sb.append(System.lineSeparator());},
                () -> sb.append(System.lineSeparator()));

        // Change listing
        currentMap.put(current.hashCode(), current);

        return sb.toString();
    }
}
