package fi.draugo.poe.lootfilter.output;

import lombok.Data;

@Data
public class OutputPacket {
    private String filter;
    private String changes;
}
