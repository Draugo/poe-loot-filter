package fi.draugo.poe.lootfilter.util;

import java.util.Objects;

public class Comment {
    private String comment;

    public Comment(String comment) {
        Objects.requireNonNull(comment, "Comment is mandatory for Comment");
        this.comment = comment;
    }

    public String print() {
        return "# " + comment;
    }
}
