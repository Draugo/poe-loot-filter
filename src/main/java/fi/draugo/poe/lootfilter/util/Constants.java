package fi.draugo.poe.lootfilter.util;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Constants {
    private static final Path source_toggles = Paths.get("./toggles.json");
    private static final Path source_colours = Paths.get("./colours.json");
    private static final Path default_source = Paths.get("./filterInit.json");
    private static final Path dsource = Paths.get("./debug_test.json");
    private static final Path dtarget = Paths.get("./debug_test.filter");
    private static final Path target_toggles = Paths.get("./toggles.filter");
    private static final Path target_colours = Paths.get("./colours.filter");
    private static final Charset charset = StandardCharsets.UTF_8;

    public static String defaultFileName() {
        return "filter";
    }
    public static Charset charset() {
        return charset;
    }
    public static Path debugSource() {
        return dsource;
    }
    public static Path debugTarget() {
        return dtarget;
    }
    public static Path getDefaultSource() {
        return default_source;
    }
}
