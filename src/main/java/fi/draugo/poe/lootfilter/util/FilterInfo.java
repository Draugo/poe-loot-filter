package fi.draugo.poe.lootfilter.util;

import fi.draugo.poe.lootfilter.enums.Strictness;
import fi.draugo.poe.lootfilter.group.Group;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class FilterInfo {
    private String name = null;
    private String targetFilename = null;
    private String description = null;
    private Strictness strictness = null;
    private List<String> initFiles = new ArrayList<>();
    private List<Group> groups = new ArrayList<>();
    private boolean noPrint = false;

    public FilterInfo() {
    }

    public Optional<String> getName() {
        return Optional.ofNullable(name);
    }

    public void setName(String name) {
        this.name = name;
    }

    public Optional<String> getTargetFilename() {
        return Optional.ofNullable(targetFilename);
    }

    public void setTargetFilename(String targetFilename) {
        this.targetFilename = targetFilename;
    }

    public Optional<String> getDescription() {
        return Optional.ofNullable(description);
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getInitFiles() {
        return initFiles;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public Optional<Strictness> getStrictness() {
        return Optional.ofNullable(strictness);
    }

    public void setStrictness(Strictness strictness) {
        this.strictness = strictness;
    }

    public boolean isNoPrint() {
        return noPrint;
    }

    public void setNoPrint(boolean noPrint) {
        this.noPrint = noPrint;
    }
}
