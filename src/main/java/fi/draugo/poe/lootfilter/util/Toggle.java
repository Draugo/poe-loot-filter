package fi.draugo.poe.lootfilter.util;

import fi.draugo.poe.lootfilter.interfaces.Manageable;
import fi.draugo.poe.lootfilter.managers.ToggleManager;
import fi.draugo.poe.lootfilter.interfaces.Command;
import fi.draugo.poe.lootfilter.interfaces.Toggleable;

public class Toggle implements Manageable, Command, Toggleable {
    private String id;
    private Boolean enabled = true;

    public void setId(String id) {
        this.id = id;
        ToggleManager.getInstance().register(id, this);
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public String getCommand() {
        StringBuilder sb = new StringBuilder();
        sb.append(id).append(": ").append(enabled);
        return sb.toString();
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }
}
