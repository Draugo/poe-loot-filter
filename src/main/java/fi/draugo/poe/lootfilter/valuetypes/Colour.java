package fi.draugo.poe.lootfilter.valuetypes;

import fi.draugo.poe.lootfilter.interfaces.Manageable;
import fi.draugo.poe.lootfilter.managers.ColourManager;
import fi.draugo.poe.lootfilter.enums.ColourType;
import fi.draugo.poe.lootfilter.interfaces.Command;
import fi.draugo.poe.lootfilter.interfaces.Commentable;
import fi.draugo.poe.lootfilter.util.Comment;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class Colour implements Manageable, Command, Commentable {
    private Integer red, green, blue, alpha;
    private ColourType type;
    private Comment comment;
    //private String id;
    //private String ref;

    public Colour() {}

    public void setRed(Integer red) {
        this.red = red;
    }

    public void setGreen(Integer green) {
        this.green = green;
    }

    public void setBlue(Integer blue) {
        this.blue = blue;
    }

    public void setAlpha(Integer alpha) {
        this.alpha = alpha;
    }

    public void setType(ColourType type) {
        this.type = type;
    }

    public void setColour(String colour) {
        if(!StringUtils.isEmpty(colour)) {
            String[] colours = colour.split(" ");
            if(colours.length >= 3) {
                red = Integer.decode(colours[0]);
                green = Integer.decode(colours[1]);
                blue = Integer.decode(colours[2]);
                if(colours.length == 4) {
                    alpha = Integer.decode(colours[3]);
                }
            }
        }
    }

    public void setComment(Comment comment) {
        this.comment = comment;
    }

    public void setId(String id) {
        if(StringUtils.hasText(id)) {
            ColourManager.getInstance().register(id, this);
        }
    }

    public void setRef(String ref) {
        //this.ref = ref;
        Optional<Colour> colour_opt = ColourManager.getInstance().load(ref);
        colour_opt.ifPresent(c -> {
            // TODO: Might be possible to streamline
            if(getRed().isEmpty()) red = c.red;
            if(getGreen().isEmpty()) green = c.green;
            if(getBlue().isEmpty()) blue = c.blue;
            if(getAlpha().isEmpty()) alpha = c.alpha;
            if(getType() == null) type = c.type;
            if(getComment().isEmpty()) comment = c.comment;
        });
    }

    public Optional<Integer> getRed() {
        return Optional.ofNullable(red);
    }

    public Optional<Integer> getGreen() {
        return Optional.ofNullable(green);
    }

    public Optional<Integer> getBlue() {
        return Optional.ofNullable(blue);
    }

    public Optional<Integer> getAlpha() {
        return Optional.ofNullable(alpha);
    }

    public ColourType getType() {
        return type;
    }

    @Override
    public Optional<Comment> getComment() {
        return Optional.ofNullable(comment);
    }

    @Override
    public String printComment() {
        StringBuilder sb = new StringBuilder();
        getComment().ifPresent(value -> sb.append(" ").append(value.print()));
        return sb.toString();
    }

    @Override
    public String getCommand() {
        Objects.requireNonNull(type, "Type is mandatory for Colour command");
        StringBuilder sb = new StringBuilder();
        sb.append(type.getCommand())
        .append(getColourValue())
        .append(printComment());
        return sb.toString();
    }

    public String getColourValue() {
        StringBuffer sb = new StringBuffer();
        asList().forEach(value -> value.ifPresent(colour -> sb.append(" ").append(colour)));
        return sb.toString();
    }

    private List<Optional<Integer>> asList() {
        return Arrays.asList(getRed(), getGreen(), getBlue(), getAlpha());
    }

    public static Predicate<Colour> isClear() {
        return c -> c.getType().equals(ColourType.CLEAR);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getRed(), getGreen(), getBlue(), getAlpha(), getType());
    }
}
