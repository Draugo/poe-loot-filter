package fi.draugo.poe.lootfilter.valuetypes;

import com.fasterxml.jackson.annotation.JsonProperty;
import fi.draugo.poe.lootfilter.managers.ToggleManager;
import fi.draugo.poe.lootfilter.interfaces.Command;
import fi.draugo.poe.lootfilter.interfaces.Toggleable;

import java.util.Objects;

public class Value implements Command, Toggleable {
    private boolean enabled = true;
    private String value;

    public Value() {}
    public Value(String value) {
        this.value = value;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @JsonProperty("enabled")
    public void setEnabled(Object enabled) {
        if(enabled instanceof Boolean) {
            this.enabled = (Boolean) enabled;
        } else if(enabled instanceof String) {
            if(Boolean.parseBoolean((String) enabled) || ((String) enabled).equalsIgnoreCase("false")) {
                this.enabled = Boolean.parseBoolean((String) enabled);
            } else {
                ToggleManager.getInstance().load((String) enabled).ifPresent(value -> this.enabled = value.isEnabled());
            }
        }
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String getCommand() {
        StringBuilder sb = new StringBuilder();
        sb.append('"').append(value).append('"');
        return sb.toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(isEnabled(), getValue());
    }
}
